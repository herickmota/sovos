import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.AbstractRobot;

public class VatTest extends AbstractRobot {

    private AccessApplication application;
    private Login login;

    @BeforeClass
    public void setup() {
        application = new AccessApplication();
        login = new Login();
    }

    @Test
    public void test() {
        application.accessVATPortal();
        login.login("automation.testing@sovos.com", "Sovos123!");
        isDisplay(By.className("nav"), 20);
    }

    @AfterClass
    public void finishTest() {
        quit();
    }

}
