package utils;

import configs.ConfigWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public abstract class AbstractRobot {

    private static WebDriver driver;

    public WebDriver getDriver() {
        if (driver == null) {
            driver = new ConfigWebDriver().getDriver("chrome");
        }
        return driver;
    }

    public WebElement findElement(By by) {
        return getDriver().findElement(by);
    }

    public List<WebElement> findElements(By by) {
        return getDriver().findElements(by);
    }

    public void click(By by) {
        findElement(by).click();
    }

    public void navegateTo(String url) {
        getDriver().navigate().to(url);
    }

    public String getText(By by) {
        return findElement(by).getText();
    }

    public void sendKeys(By by, CharSequence... charSequences) {
        findElement(by).sendKeys(charSequences);
    }

    public void refreshPage() {
        getDriver().navigate().refresh();
    }

    public void scrollToElement(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("argument[0].scrollIntoView(true);", element);
    }

    public void quit() {
        getDriver().quit();
    }

    public void submit(By by) {
        findElement(by).submit();
    }

    protected WebElement isDisplay(By by, long timeOutInSeconds) {
        return new WebDriverWait(getDriver(), timeOutInSeconds)
                .until(ExpectedConditions.presenceOfElementLocated(by));
    }

}
