package configs;

import org.openqa.selenium.WebDriver;

public class ConfigWebDriver {

    private WebDriver driver;

    public WebDriver getDriver(String browser) {
        if (driver == null) {

            switch (browser) {
                case "ie":
                    System.setProperty("webdriver.ir.driver", "cobnfiguracoes/MicrosoftWebDriver.exe");
                    driver = new CreateIEDriver().createWebDriver();
                    break;
                default:
                    return new CreateChromeDriver().createWebDriver();
            }

        }


        return driver;
    }
}
