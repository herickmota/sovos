import utils.AbstractRobot;

import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.xpath;

public class Login extends AbstractRobot {

    public void login(String user, String pass) {
        sendKeys(id("Email"), user);
        sendKeys(id("Password"), pass);
        submit(xpath("//button[@type='submit']"));
    }
}
