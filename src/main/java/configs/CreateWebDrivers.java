package configs;

import org.openqa.selenium.WebDriver;

public interface CreateWebDrivers {
    WebDriver createWebDriver();
}
